# Lexicon Mining, Language Visualization and Semiotic Squares in Python

### Meeting Anouncement
**Classy words:** formulas for finding category-associated terms
by Jason S. Kessler

ATOM is meeting on Tuesday, 13th March, 6:30pm, at Galvanize!

Join us for a highly participative discussion !
FYI, this will be an expansion of the recent PuPPy talk so some material will be repeated. We'll look critically at a number of formulas which identify how associated a word is to a document category given a labeled corpus. This will include simple frequency differences, variations of tf.idf, variations of the log-odds-ratio, Scaled F-Score, and potentially others. Before the session, a notebook containing examples of these formulas in action will be posted.

**Recommended Readings:**  
http://languagelog.ldc.upenn.edu/myl/Monroe.pdf (up to the end of 3.5.1)
https://web.stanford.edu/~jurafsky/slp3/18.pdf
Jason S. Kessler. Scattertext: a Browser-Based Tool for Visualizing how Corpora Differ. ACL System Demonstrations. 2017. arxiv.org/abs/1703.00565

**About ATOM:**  
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !

### Original Repo
https://github.com/JasonKessler/PuPPyTalk

### Original README content
February 21, 2018 
Talk to the Puget Sound Python Programming Group

Please see [Kessler-Puppy-2018-02-21.pptx](Kessler-Puppy-2018-02-21.pptx) for some introductory slides, and a brief survey of psychological literature on the importance of function words in lexicon mining.

The two notebooks used are written in Python 3.6.  Please run

$ pip install scattertext spacy gensim

before using them.  

The first notebook, [Class-Association-Scores.ipynb](http://nbviewer.jupyter.org/github/JasonKessler/PuPPyTalk/blob/master/notebooks/Class-Association-Scores.ipynb), demonstrates a how to use Scattertext to visualize term-category assocations.  The notebook will motivate and introduce the "Fightin' Words" formula-- the Log-Odds-Ratio with an Informative Dirichlet Prior (Monroe et al. 2008).  The notebook goes on to discuss Scaled F-Score and the Dense Rank Difference. Data will be used from Pang et al., 2002.  

The second notebook, [Explore-Headlines.ipynb](http://nbviewer.jupyter.org/github/JasonKessler/PuPPyTalk/blob/master/notebooks/Explore-Headlines.ipynb), shows how to use Scattertext to visualize the interactions between a number of document categories.  The example used will be headlines posted to Facebook accounts from a variety of publishers in 2016. The data is taken verbatim from Max Woolfe's data set, available at https://github.com/minimaxir/clickbait-cluster under the MIT license.

I've included a notebook exploring toxic comment classification from a recent Kaggle competition: [Toxic-Comments](http://nbviewer.jupyter.org/github/JasonKessler/PuPPyTalk/blob/master/notebooks/Toxic-Comments.ipynb).

# References
* Cindy K. Chung and James W. Pennebaker. 2012. Counting Little Words in Big Data: The Psychology of Communities, Culture, and History. EASP.
* Susan C. Herring, Anna Martinson. 2004. Assessing Gender Authenticity in Computer-Mediated Language Use: Evidence From an Identity Game. Journal of Language and Social Psychology. 
* Dan Jurafsky, Victor Chahuneau, Bryan Routledge, and Noah Smith. Narrative framing of consumer sentiment in online restaurant reviews. First Monday. 2014.
* Jason S. Kessler. 2017. Scattertext: a Browser-Based Tool for Visualizing how Corpora Differ. ACL System Demonstrations. 
* McInnes, L, Healy, J, UMAP: Uniform Manifold Approximation and Projection for Dimension Reduction, ArXiv e-prints 1802.03426, 2018.
* Burt L. Monroe, Michael P. Colaresi, and Kevin M. Quinn. 2008. Fightin’ words: Lexical feature selection and evaluation for identifying the content of political conflict. Political Analysis.
* Newman, ML; Groom, CJ; Handelman LD, Pennebaker, JW. Gender Differences in Language Use: An Analysis of 14,000 Text Samples. 2008.
* Bo Pang, Lillian Lee, and Shivakumar Vaithyanathan. 2002. Thumbs up? Sentiment Classification using Machine Learning Techniques, EMNLP.
* James W. Pennebaker, Carla J. Groom, Daniel Loew, James M. Dabbs.  2004. Testosterone as a Social Inhibitor: Two Case Studies of the Effect of Testosterone Treatment on Language. J Abnorm Psychol.

